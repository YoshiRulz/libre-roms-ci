{ lib
, stdenv
, fetchFromGitHub
, rgbds # >= 0.5.1
, which
}:
stdenv.mkDerivation (finalAttrs: {
	pname = "pokegold";
	version = "0.1.0+a9dd70cc3";
	src = fetchFromGitHub {
		owner = "pret";
		repo = finalAttrs.pname;
		rev = "a9dd70cc35acee8b5eb701da66d82ba74a1491c2";
		hash = "sha512-QLuMXYgXen3gU6JBbR9dsxXr8GkEjP18+E6gUHgqL2JID+vbvEax/4ea57ozAwOU9qqttCGVNPLyeM1fUdIZog==";
	};
	nativeBuildInputs = [ stdenv rgbds which ];
	dontConfigure = true;
	postBuild = ''
		make compare
	'';
	installPhase = ''
		mkdir -p -v $out
		for f in *.gbc; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "(Assembled) disassembly of Pokémon Gold/Silver";
		homepage = "https://github.com/pret/pokegold";
		license = lib.licenses.unfree; # decompilation of closed source binaries
		platforms = lib.platforms.all;
	};
})
