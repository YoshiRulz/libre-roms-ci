{ lib
, stdenv
, fetchFromGitHub
, rgbds # >= 0.5.1
, which
}:
stdenv.mkDerivation (finalAttrs: {
	pname = "pokeyellow";
	version = "0.1.0+0bb0c76ab";
	src = fetchFromGitHub {
		owner = "pret";
		repo = finalAttrs.pname;
		rev = "0bb0c76abf63b8cf86874fab2621a65b6b293b94";
		hash = "sha512-bTdwHtr73LAZpARIKKkvT2l39LK9YfjsIjCKu85CKBXAUZuszMXyMz63uaet7jIPzWfIn1Tw8TRYdokXxEiYig==";
	};
	nativeBuildInputs = [ stdenv rgbds which ];
	dontConfigure = true;
	postBuild = ''
		make compare
	'';
	installPhase = ''
		mkdir -p -v $out
		for f in *.gbc; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "(Assembled) disassembly of Pokemon Yellow";
		homepage = "https://github.com/pret/pokeyellow";
		license = lib.licenses.unfree; # decompilation of closed source binaries
		platforms = lib.platforms.all;
	};
})
