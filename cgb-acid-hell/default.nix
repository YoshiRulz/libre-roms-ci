{ lib
, stdenv
, fetchFromGitHub
, rgbds
, which
}:
stdenv.mkDerivation {
	pname = "cgb-acid-hell";
	version = "0.1.0+107b7c5a8";
	src = fetchFromGitHub {
		owner = "mattcurrie";
		repo = "cgb-acid-hell";
		rev = "107b7c5a875f26473ebc1193e32c59394bdd3049";
		hash = "sha512-VWrCZ+zy3DAoBFM5R06tbBkc9IU4tCamPrYxdMBgpNkTM/0iIliiTeor+Dq0k7tCRcugGldlRMWQUXmv5yM1SA==";
		fetchSubmodules = true;
	};
	nativeBuildInputs = [ stdenv rgbds which ];
	postPatch = ''
		sed -i "s/md5/md5sum/" Makefile
	'';
	dontConfigure = true;
	installPhase = ''
		mkdir -p -v $out
		for f in cgb-acid-hell.* img/*; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "Your Game Boy Color emulator does not pass this test.";
		homepage = "https://github.com/mattcurrie/cgb-acid-hell";
		license = lib.licenses.mit;
		platforms = lib.platforms.all;
	};
}
