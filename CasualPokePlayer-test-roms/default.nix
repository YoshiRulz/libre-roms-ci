{ lib
, stdenv
, fetchFromGitHub
, python
, rgbds
, which
}:
let
	pname = "CasualPokePlayer-test-roms";
	sourceInfos = [
		{
			name = "gb-camera-test";
			rev = "ae16bb53dbc5cd9fba20709e14c6a2690a095aef";
			hash = "sha512-5l5NiJWFLo6bN2//8ZgLjdwzFkob1BowC6Xnj7J9foZkIh7bv2yQFHdsITqTitG7QE3zlR7VTcmXP/O9lrsXJA==";
		}
		{
			name = "open-bus-ss-test";
			rev = "e38ef460a10c0f4e0ba49e449a93c0650cc341eb";
			hash = "sha512-whhpJkzTjwrDWYhaCG8csLozoKjTPFmX329I0hvv+Zkd3gpaShVygugXFj46Bmfh8QM/FMshoHi6iSDaNS4EzQ==";
		}
		{
			name = "ramg-mbc3-test";
			rev = "f55c82a0f64e7371eea43a22590b1d267f2e5def";
			hash = "sha512-aX3gQ40UNE6+w50OlLOzbe+g3S3dorR6Vj0OfjPjrt+J5jB/4BBqKXBh7aryCvvcBijJO+imJBIukyPX454UGg==";
		}
		{
			name = "rtc-invalid-banks-test";
			rev = "708634cda2ba7b5a64d9c0ea0ff1d7dca24048d1";
			hash = "sha512-9PGtaJ02ZiaPP+2T204z7IeQAx3/McfgNMWeglBHqdsXY2ghpjRb6jxWH+BJ0Z9hkDD1EKe6rKbFgcbXIGIBAg==";
		}
		{
			name = "sgb-mlt-test";
			rev = "1b14bfbfbeed95b30885661f865d51c5c0cf924e";
			hash = "sha512-K7PxAwtn5mr2LB0ZACI8JHH+VfoQPmIt2R4WLFXVWlpR0TQfZQH/SgFW+IN8aE8ximxGwvv71SpPavCsWfqSGw==";
		}
	];
	combinedSources = stdenv.mkDerivation {
		pname = "${pname}-combinedsrc";
		version = "0.1.0+1b14bfbfb"; # hash of most recent commit
		srcs = builtins.map (args: fetchFromGitHub (args // {
			owner = "CasualPokePlayer";
			repo = "test-roms";
			fetchSubmodules = true;
		})) sourceInfos;
		nativeBuildInputs = [ stdenv ];
		setSourceRoot = ''
			mkdir -p source
			for d in ${lib.concatStringsSep " " (map (a: a.name) sourceInfos)}; do
				cp -a -t source $d
			done
			export sourceRoot=source
		'';
		dontPatch = true;
		dontConfigure = true;
		dontBuild = true;
		installPhase = ''
			mkdir -p $out
			cp -a -t $out *
		'';
	};
in stdenv.mkDerivation {
	inherit pname;
	version = combinedSources.version;
	srcs = [
		combinedSources
		(builtins.path { path = ./img; name = "img"; })
	];
	nativeBuildInputs = [ stdenv python rgbds which ];
	setSourceRoot = ''
		mkdir -p source
		cp -a -t source ${combinedSources.pname}-*/*
		cp -a -t source img/*
		export sourceRoot=source
	'';
	postPatch = ''
		patchShebangs --build */src/tools/pb8.py
	'';
	dontConfigure = true;
	buildPhase = ''
		r=$PWD
		for d in *; do if [ -d "$d" ]; then
			printf "building branch %s" $d
			cd $d
			make
			cd $r
		fi; done
	'';
	installPhase = ''
		mkdir -p -v $out
		for f in */bin/* *.png; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "A collection of test ROMs for the GB/C";
		homepage = "https://github.com/CasualPokePlayer/test-roms";
		license = lib.licenses.mit;
		platforms = lib.platforms.all;
	};
}
